const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(button).toBe('Join');
    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Admin', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Room', {delay: 100});
    await page.keyboard.press('Enter', {delay: 500}); 
    await page.screenshot({path: 'test/screenshots/welcome.png'});
    await browser.close();

})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.keyboard.press('Enter', {delay: 500}); 
    await page.screenshot({path: 'test/screenshots/error.png'});
    await browser.close();

})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Room', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    
    await page.waitForSelector('#messages > li > div.message__body > p');    
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li > div.message__body > p').innerHTML;
    });
    expect(message).toBe('Hi John, Welcome to the chat app');
    await browser.close();

})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R00m', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(userName).toBe('R00m');

    await browser.close();

})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');

    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser.newPage();
    
    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'John2', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page2.waitForSelector('#users > ol > li:nth-child(2)');

    await browser.close();
    await browser2.close();

})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    let button = await page.$eval('body > div:nth-child(2) > div > form > button', (content) => content.innerHTML);
    expect(button).toBe('Send');
    await browser.close();

})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    
    //let message = await page.$eval('body', (content) => content.innerHTML);
    //console.log(message)
    
    //await page.type('body > div:nth-child(2) > div > form > input[type=text]', 'Hi!', {delay: 100});

    await page.waitForSelector('#message-form > input[type=text]');
    await page.type('#message-form > input[type=text]', 'Hello!', {delay: 100});
	
    await page.waitForSelector('#message-form > button');
    await page.waitFor(2000);
    //await page.focus('#message-form > input[type=text]')
    await page.click('#message-form > button', {delay: 100}); 
    
    await page.waitForSelector('#messages > li:nth-child(2) > div.message__body > p');
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div.message__body > p').innerHTML;
    });
    expect(message).toBe('Hello!');
    await browser.close();
    //await browser.close()
})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    // John login
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'J&M', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
     
    // Mike login
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser.newPage();
    
    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'J&M', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page2.waitForSelector('#users > ol > li:nth-child(2)');
    
    // John say Hi
    await page.bringToFront();
    await page.waitForSelector('#message-form > input[type=text]');
    await page.type('#message-form > input[type=text]', 'Hi', {delay: 100});
    await page.waitForSelector('#message-form > button');
    await page.waitFor(2000);
    await page.click('#message-form > button', {delay: 100}); 

    // Check John's message had arrived
    await page2.bringToFront();
    await page2.waitForSelector('#messages > li:nth-child(2) > div.message__title');
    let John = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div.message__title > h4').innerHTML;
    });

    await page2.waitForSelector('#messages > li:nth-child(2) > div.message__body > p');
    let John_message = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div.message__body > p').innerHTML;
    });
    expect(John).toBe('John');
    expect(John_message).toBe('Hi');

   // Mike say Hello
    await page2.waitForSelector('#message-form > input[type=text]');
    await page2.type('#message-form > input[type=text]', 'Hello', {delay: 100});
    await page2.waitForSelector('#message-form > button');
    await page2.waitFor(2000);
    await page2.click('#message-form > button', {delay: 100}); 
    
    // Check Mike's message had arrived
    await page.bringToFront();
    await page.waitForSelector('#messages > li:nth-child(4) > div.message__title');
    let Mike = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div.message__title > h4').innerHTML;
    });

    await page.waitForSelector('#messages > li:nth-child(4) > div.message__body > p');
    let Mike_message = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div.message__body > p').innerHTML;
    });
    expect(Mike).toBe('Mike');
    expect(Mike_message).toBe('Hello');
    await browser.close();
    await browser2.close();
    
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Johnson', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'JJJ', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    

    await page.waitForSelector('#send-location');    
    let location_button = await page.evaluate(() => {
        return document.querySelector('#send-location').innerHTML;
    });
    expect(location_button).toBe('Send location');
    await browser.close();
})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);
    const context = browser.defaultBrowserContext();
    await context.overridePermissions(url, ['geolocation']);
    
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Johnson', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'JJJ', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    
    await page.evaluate(() => {
  	localStorage.setItem('token', 'enable');
    });
    
    await page.waitForSelector('#send-location');  
    await page.waitFor(2000);
    await page.click('#send-location', {delay: 100}); 
    await page.waitFor(2000);
    await page.waitForSelector('#send-location');    
    let location_button = await page.evaluate(() => {
        return document.querySelector('#send-location').innerHTML;
    });
    expect(location_button).toBe('Sending location...');
    

})
